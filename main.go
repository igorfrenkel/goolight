package main

import (
	"fmt"
	"log"
	"net/http"
	"os/exec"
	"strconv"
)

var frontHtml = `<html>
<head>
	<title>goolight</title>
	 <meta name="viewport" content="width=device-width, initial-scale=1"> 
</head>
<body>
<h1>Light control</h1>
<hr/>
<div>
	<h2>Goo room bulb 1</h2>
	<p>
		<h3>Change power state</h3>
		<a href="/power?room=gooroom1&state=on">lights on</a>
		<br/>
		<a href="/power?room=gooroom1&state=off">lights off</a>
	</p>
	<p>
		<h3>Set brightness (by percent intensity)</h3>
		<a href="/brightness?room=gooroom1&level=1">1</a>
		<a href="/brightness?room=gooroom1&level=5">5</a>
		<a href="/brightness?room=gooroom1&level=10">10</a>
		<a href="/brightness?room=gooroom1&level=15">15</a>
		<a href="/brightness?room=gooroom1&level=20">20</a>
		<a href="/brightness?room=gooroom1&level=30">30</a>
		<a href="/brightness?room=gooroom1&level=40">40</a>
		<a href="/brightness?room=gooroom1&level=50">50</a>
		<a href="/brightness?room=gooroom1&level=60">60</a>
		<a href="/brightness?room=gooroom1&level=70">70</a>
		<a href="/brightness?room=gooroom1&level=80">80</a>
		<a href="/brightness?room=gooroom1&level=90">90</a>
		<a href="/brightness?room=gooroom1&level=100">100</a>
	</p>
	<p>
		<form action="/fade">
			<input type="hidden" name="room" value="gooroom1"/>
			<h3>Fade to
				<select name="start">
					<option>100</option>
					<option>90</option>
					<option>80</option>
					<option>70</option>
					<option>60</option>
					<option>50</option>
					<option>40</option>
					<option>30</option>
					<option>20</option>
					<option>15</option>
					<option>10</option>
					<option>5</option>
					<option>1</option>
				</select>
				percent in
				<select name="duration">
					<option>3600</option>
					<option>2700</option>
					<option>1800</option>
					<option>1200</option>
					<option>600</option>
					<option>180</option>
					<option>120</option>
					<option>60</option>
					<option>30</option>
					<option>20</option>
					<option>15</option>
					<option>10</option>
					<option>5</option>
				</select>
				seconds
				<input type="submit" value="fade" />
			</h3>
		</form>
	</p>
</div>
<hr/>
<div>
	<h2>Goo room bulb 2</h2>
	<p>
		<h3>Change power state</h3>
		<a href="/power?room=gooroom2&state=on">lights on</a>
		<br/>
		<a href="/power?room=gooroom2&state=off">lights off</a>
	</p>
	<p>
		<h3>Set brightness (by percent intensity)</h3>
		<a href="/brightness?room=gooroom2&level=1">1</a>
		<a href="/brightness?room=gooroom2&level=5">5</a>
		<a href="/brightness?room=gooroom2&level=10">10</a>
		<a href="/brightness?room=gooroom2&level=15">15</a>
		<a href="/brightness?room=gooroom2&level=20">20</a>
		<a href="/brightness?room=gooroom2&level=30">30</a>
		<a href="/brightness?room=gooroom2&level=40">40</a>
		<a href="/brightness?room=gooroom2&level=50">50</a>
		<a href="/brightness?room=gooroom2&level=60">60</a>
		<a href="/brightness?room=gooroom2&level=70">70</a>
		<a href="/brightness?room=gooroom2&level=80">80</a>
		<a href="/brightness?room=gooroom2&level=90">90</a>
		<a href="/brightness?room=gooroom2&level=100">100</a>
	</p2>
	<p>
		<form action="/fade">
			<input type="hidden" name="room" value="gooroom2"/>
			<h3>Fade to
				<select name="start">
					<option>100</option>
					<option>90</option>
					<option>80</option>
					<option>70</option>
					<option>60</option>
					<option>50</option>
					<option>40</option>
					<option>30</option>
					<option>20</option>
					<option>15</option>
					<option>10</option>
					<option>5</option>
					<option>1</option>
				</select>
				percent in
				<select name="duration">
					<option>3600</option>
					<option>2700</option>
					<option>1800</option>
					<option>1200</option>
					<option>600</option>
					<option>180</option>
					<option>120</option>
					<option>60</option>
					<option>30</option>
					<option>20</option>
					<option>15</option>
					<option>10</option>
					<option>5</option>
				</select>
				seconds
				<input type="submit" value="fade" />
			</h3>
		</form>
	</p>
</div>
<hr/>
<div>
	<h2>Living room bulb</h2>
	<p>
		<h3>Change power state</h3>
		<a href="/power?room=lroom&state=on">lights on</a>
		<br/>
		<a href="/power?room=lroom&state=off">lights off</a>
	</p>
	<p>
		<h3>Set brightness (percent intensity)</h3>
		<a href="/brightness?room=lroom&level=1">1</a>
		<a href="/brightness?room=lroom&level=5">5</a>
		<a href="/brightness?room=lroom&level=10">10</a>
		<a href="/brightness?room=lroom&level=15">15</a>
		<a href="/brightness?room=lroom&level=20">20</a>
		<a href="/brightness?room=lroom&level=30">30</a>
		<a href="/brightness?room=lroom&level=40">40</a>
		<a href="/brightness?room=lroom&level=50">50</a>
		<a href="/brightness?room=lroom&level=60">60</a>
		<a href="/brightness?room=lroom&level=70">70</a>
		<a href="/brightness?room=lroom&level=80">80</a>
		<a href="/brightness?room=lroom&level=90">90</a>
		<a href="/brightness?room=lroom&level=100">100</a>
	</p>
</div>
</body>
</html>`

func front(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, frontHtml)
}

func power(w http.ResponseWriter, r *http.Request) {
	if keys, ok := r.URL.Query()["state"]; ok {
		cmd := exec.Command("bash", "-c", fmt.Sprintf("./goolight.sh %s %s", room(r), keys[0]))
		res, err := cmd.CombinedOutput()
		log.Print(string(res), err)
	}
	http.Redirect(w, r, "/", http.StatusFound)
}

func brightness(w http.ResponseWriter, r *http.Request) {
	if keys, ok := r.URL.Query()["level"]; ok {
		pct, err := strconv.ParseFloat(keys[0], 32)
		if err != nil {
			log.Print("error parse float", err)
		} else {
			log.Print("pct ", pct)
			brightness := int(254 * (pct / 100))
			log.Print("brightness ", brightness)
			cmd := exec.Command("bash", "-c", fmt.Sprintf("./goolight.sh %s brightness %d", room(r), brightness))
			res, err := cmd.CombinedOutput()
			log.Print(string(res), err)
		}
	}
	http.Redirect(w, r, "/", http.StatusFound)
}

var rooms = map[string]bool{
	"lroom":    true,
	"gooroom1": true,
	"gooroom2": true,
}

const defaultRoom = "lroom"

func room(r *http.Request) string {
	if keys, ok := r.URL.Query()["room"]; ok {
		if _, ok := rooms[keys[0]]; ok {
			return keys[0]
		}
	}
	return defaultRoom
}

func fade(w http.ResponseWriter, r *http.Request) {
	log.Print("query", r.URL.Query())
	keys, ok := r.URL.Query()["start"]
	if !ok {
		log.Print("brightness start pct missing")
		http.Redirect(w, r, "/", http.StatusFound)
	}
	pct, err := strconv.ParseFloat(keys[0], 32)
	if err != nil {
		log.Print("error parse float", err)
	}
	keys, ok = r.URL.Query()["duration"]
	if !ok {
		log.Print("duration value missing")
		http.Redirect(w, r, "/", http.StatusFound)
	}
	dur, err := strconv.ParseInt(keys[0], 10, 32)
	if err != nil {
		log.Print("error parse int", err)
	}
	lvl := int(254 * (pct / 100))
	log.Print("brightness: %s pct, %s lvl", pct, lvl)
	log.Print("dur ", dur)
	log.Print("cmd", fmt.Sprintf("./trigger.sh %s fade %d %d", room(r), lvl, dur))
	cmd := exec.Command("bash", "-c", fmt.Sprintf("./trigger.sh %s fade %d %d", room(r), lvl, dur))
	res, err := cmd.CombinedOutput()
	log.Print(string(res), err)
	http.Redirect(w, r, "/", http.StatusFound)
}

func main() {
	http.HandleFunc("/", front)
	http.HandleFunc("/power", power)
	http.HandleFunc("/brightness", brightness)
	http.HandleFunc("/fade", fade)
	log.Fatal(http.ListenAndServe(":8081", nil))
}
