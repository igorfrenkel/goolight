#!/usr/bin/env bash

goo_lamp_colour="FA_7D_3B_18_3D_AA"
goo_lamp_white="C1_94_FB_C9_61_35"
lroom_lamp_white="F6_EA_44_13_0C_37"

array_lamps=( ${goo_lamp_white} )
if [ "${1}" == "gooroom1" ]; then
	array_lamps=( ${goo_lamp_white} )
elif [ "${1}" == "gooroom2" ]; then
	array_lamps=( ${goo_lamp_colour} )
else 
	array_lamps=( ${lroom_lamp_white} )
fi

srcdir=$(dirname $BASH_SOURCE)

. "${srcdir}/goolib.sh"
[[ $1 == "--debug" ]] || [[ $1 == "-d" ]] && . $(characteristic_values)

connection_test
echo "args:" $@
case "${2}" in
	"on")
		send "0x01"
		;;
	"off")
		send "0x00"
		;;
	"brightness")
		send "0x01"
		set_brightness "${3}"
		;;
	"fade")
		fade "$3" "$4"
		;;
	*)
		echo "invalid command"
		exit
		;;
esac
