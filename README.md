# googlight

## installation

todo

## todos

### immediate changes

- [ ] push debby changes to remote repo
- [ ] checkout on r3pi
- [ ] disconnect light bulbs from debby
- [ ] connect lightbulbs on r3pi
- [ ] install dbus
- [ ] run goolight
- [ ] note installtion instructions

### desired changes

- [ ] connect to light bulbs to go rather than bash script

### functionality improvements

- [ ] control one bulb
- [ ] control both bulbs
- [ ] turn light on/off
- [ ] turn light to 100% and 0%
- [ ] fade light up/down over time
- [ ] wake up sequence
- [ ] sleep sequence
- [ ] link to web server
