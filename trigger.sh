#!/bin/bash

GOOLOG=/tmp/goolight.log
GOOPID=/tmp/goolight.pid

log() {
  prefix="[$(date)][trigger]"
  msg="$prefix $@"
  echo "$prefix $@" >> $GOOLOG
}

list_descendants () {
  local children=$(ps -o pid= --ppid "$1")

	for pid in $children
  do
    list_descendants "$pid"
  done

  echo "$children"
}

# TODO: switch to pgrep
kill_family() {
  local children=$(list_descendants $1)
  kill $children || true
  kill $1 || true
}

if [ -f "$GOOPID" ]; then
  pid=$(cat $GOOPID)
	if [ ! -z $pid ]; then
		log "found existing goolight execution with pid $pid"
		kill_family $pid || log "error stopping pid: $pid (exit code: $?)"
	fi
fi

# TODO: check if not running and exit with error
bash goolight.sh $@ &>/tmp/goolight.log &
pid=$!
log "sending goo light command (args: $@) on pid $pid"
echo $pid > $GOOPID
