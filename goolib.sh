#!/bin/bash

export DBUS_LOCATION=/usr/local/bin


connection_test(){

	for lamp in ${array_lamps[@]}
	do
		con=`$DBUS_LOCATION/dbus-send --print-reply --system --dest=org.bluez /org/bluez/hci0/dev_${lamp} org.freedesktop.DBus.Properties.Get string:"org.bluez.Device1" string:"Connected"`
		con_bool=${con##* }

		if [[ "${con_bool}" == "false" ]]
		then
			echo "connection status of ${lamp} = connecting"
			$DBUS_LOCATION/dbus-send --print-reply --system --dest=org.bluez /org/bluez/hci0/dev_${lamp} org.bluez.Device1.Connect
			con=`$DBUS_LOCATION/dbus-send --print-reply --system --dest=org.bluez /org/bluez/hci0/dev_${lamp} org.freedesktop.DBus.Properties.Get string:"org.bluez.Device1" string:"Connected"`
			con_bool=${con##* }
			echo "connection status of ${lamp} = ${con_bool}"
		else
			echo "connection status of ${lamp} = ${con_bool}"
		fi
	done
	echo
}

characteristic_values() {
	for lamp in ${array_lamps[@]}
	do
		$DBUS_LOCATION/dbus-send --print-reply --system --dest=org.bluez /org/bluez/hci0/dev_${lamp}/service0023/char0029 org.bluez.GattCharacteristic1.ReadValue dict:string:variant: | grep 'array of bytes' -A 1 | tail -1
	done

	echo -e '\n'
	echo "######################################"
	echo ">>>>>> Manufacturer Name String <<<<<<"
	echo "######################################"
	echo "/service000a/char000b"
	for lamp in ${array_lamps[@]}
	do
		$DBUS_LOCATION/dbus-send --print-reply --system --dest=org.bluez /org/bluez/hci0/dev_${lamp}/service000a/char000b org.bluez.GattCharacteristic1.ReadValue dict:string:variant:
	done
	echo -e '\n'
	echo "######################################"
	echo ">>>>>>> Model Number String <<<<<<<<< "
	echo "######################################"
	echo "/service000a/char000d"
	for lamp in ${array_lamps[@]}
	do
		$DBUS_LOCATION/dbus-send --print-reply --system --dest=org.bluez /org/bluez/hci0/dev_${lamp}/service000a/char000d org.bluez.GattCharacteristic1.ReadValue dict:string:variant:
	done
	echo -e '\n'
	echo "######################################"
	echo ">>>>> Software Revision String <<<<<<<"
	echo "######################################"
	echo "/service000a/char000f"
	for lamp in ${array_lamps[@]}
	do
		$DBUS_LOCATION/dbus-send --print-reply --system --dest=org.bluez /org/bluez/hci0/dev_${lamp}/service000a/char000f org.bluez.GattCharacteristic1.ReadValue dict:string:variant:
	done
	echo -e '\n'
	echo "######################################"
	echo ">>>>>>>>> Lamp Name, alias <<<<<<<<<<<"
	echo "######################################"
	echo "/service0011/char0014"
	for lamp in ${array_lamps[@]}
	do
		$DBUS_LOCATION/dbus-send --print-reply --system --dest=org.bluez /org/bluez/hci0/dev_${lamp}/service0011/char0014 org.bluez.GattCharacteristic1.ReadValue dict:string:variant:
	done
	echo -e '\n'
	echo "########################################################################################"
	echo "> i guess Pairable state after AC-ON. After AC-ON the value changes to 02 for a while. <"
	echo "########################################################################################"
	echo "/service0011/char001b"
	for lamp in ${array_lamps[@]}
	do
		$DBUS_LOCATION/dbus-send --print-reply --system --dest=org.bluez /org/bluez/hci0/dev_${lamp}/service0011/char001b org.bluez.GattCharacteristic1.ReadValue dict:string:variant:
	done
	echo -e '\n'
	echo "######################################"
	echo ">>>>>>> read posible but ??? <<<<<<<<<"
	echo "######################################"
	echo "/service0023/char0024"
	for lamp in ${array_lamps[@]}
	do
		$DBUS_LOCATION/dbus-send --print-reply --system --dest=org.bluez /org/bluez/hci0/dev_${lamp}/service0023/char0024 org.bluez.GattCharacteristic1.ReadValue dict:string:variant:
	done
	echo -e '\n'
	echo "######################################"
	echo ">>>>>>>>>>>>> on/off <<<<<<<<<<<<<<<<<"
	echo "######################################"
	echo "/service0023/char0026"
	for lamp in ${array_lamps[@]}
	do
		$DBUS_LOCATION/dbus-send --print-reply --system --dest=org.bluez /org/bluez/hci0/dev_${lamp}/service0023/char0026 org.bluez.GattCharacteristic1.ReadValue dict:string:variant:
	done
	echo -e '\n'
	echo "######################################"
	echo ">>>>>>>>>>> brightness <<<<<<<<<<<<<<<"
	echo "######################################"
	echo "/service0023/char0029"
	for lamp in ${array_lamps[@]}
	do
		$DBUS_LOCATION/dbus-send --print-reply --system --dest=org.bluez /org/bluez/hci0/dev_${lamp}/service0023/char0029 org.bluez.GattCharacteristic1.ReadValue dict:string:variant:
	done
	echo -e '\n'
	echo "######################################"
	echo ">>>>>>>>>>>>> mired <<<<<<<<<<<<<<<<<<"
	echo "######################################"
	echo "/service0023/char002c"
	for lamp in ${array_lamps[@]}
	do
		$DBUS_LOCATION/dbus-send --print-reply --system --dest=org.bluez /org/bluez/hci0/dev_${lamp}/service0023/char002c org.bluez.GattCharacteristic1.ReadValue dict:string:variant:
	done
	echo -e '\n'
	echo "######################################"
	echo ">>>>>>>>>>>>>> color <<<<<<<<<<<<<<<<<"
	echo "######################################"
	echo "/service0023/char002f"
	for lamp in ${array_lamps[@]}
	do
		$DBUS_LOCATION/dbus-send --print-reply --system --dest=org.bluez /org/bluez/hci0/dev_${lamp}/service0023/char002f org.bluez.GattCharacteristic1.ReadValue dict:string:variant:
	done
	echo -e '\n'
	echo "######################################"
	echo ">> on/off, brightness, mired, color <<"
	echo "######################################"
	echo "/service0023/char0034"
	for lamp in ${array_lamps[@]}
	do
		$DBUS_LOCATION/dbus-send --print-reply --system --dest=org.bluez /org/bluez/hci0/dev_${lamp}/service0023/char0034 org.bluez.GattCharacteristic1.ReadValue dict:string:variant:
	done
	echo -e '\n'
	echo "######################################"
	echo ">>>>>>>>> transitiontime <<<<<<<<<<<<<"
	echo "######################################"
	echo "/service0023/char0037"
	for lamp in ${array_lamps[@]}
	do
		$DBUS_LOCATION/dbus-send --print-reply --system --dest=org.bluez /org/bluez/hci0/dev_${lamp}/service0023/char0037 org.bluez.GattCharacteristic1.ReadValue dict:string:variant:
	done
	echo -e '\n'
	echo "###################################################"
	echo "> AC_ON values : on/off, brightness, mired, color <"
	echo "###################################################"
	echo "/service0023/char0039"
	for lamp in ${array_lamps[@]}
	do
		$DBUS_LOCATION/dbus-send --print-reply --system --dest=org.bluez /org/bluez/hci0/dev_${lamp}/service0023/char0039 org.bluez.GattCharacteristic1.ReadValue dict:string:variant:
	done
	echo -e '\n'
	echo "######################################"
	echo ">>>>>>>>>>> ???  ??? <<<<<<<<<<<<<<<<<"
	echo "######################################"
	echo "/service003b/char003c"
	for lamp in ${array_lamps[@]}
	do
		$DBUS_LOCATION/dbus-send --print-reply --system --dest=org.bluez /org/bluez/hci0/dev_${lamp}/service003b/char003c org.bluez.GattCharacteristic1.ReadValue dict:string:variant:
	done
	echo -e '\n'
	echo "######################################"
	echo ">>>>>> send 02 00 posible <<<<<<<<<<<<"
	echo "######################################"
	echo "/service003b/char003e/desc0040"
	for lamp in ${array_lamps[@]}
	do
		$DBUS_LOCATION/dbus-send --print-reply --system --dest=org.bluez /org/bluez/hci0/dev_${lamp}/service003b/char003e/desc0040 org.bluez.GattDescriptor1.ReadValue dict:string:variant:
	done
	echo -e '\n'
	echo "######################################"
	echo ">>>>>>>>>>>> ???  ??? <<<<<<<<<<<<<<<<"
	echo "######################################"
	echo "/service003b/char0043"
	for lamp in ${array_lamps[@]}
	do
		$DBUS_LOCATION/dbus-send --print-reply --system --dest=org.bluez /org/bluez/hci0/dev_${lamp}/service003b/char0043 org.bluez.GattCharacteristic1.ReadValue dict:string:variant:
	done

}

send() {
	for lamp in ${array_lamps[@]}
	do
		$DBUS_LOCATION/dbus-send --print-reply --system --dest=org.bluez /org/bluez/hci0/dev_${lamp}/service0023/char0026 org.bluez.GattCharacteristic1.WriteValue array:byte:"${1}" dict:string:variant:
	done
}

set_brightness() {
	
	let val=$1
	bri="0x$(printf '%02x' "${val}")"

	echo ${val}
	echo ${bri}

	for lamp in ${array_lamps[@]}
	do
		$DBUS_LOCATION/dbus-send --print-reply --system --dest=org.bluez /org/bluez/hci0/dev_${lamp}/service0023/char0029 org.bluez.GattCharacteristic1.WriteValue array:byte:"${bri}" dict:string:variant:
	done

	echo
}

get_brightness() {
	let max=0
	let bri=0
	for lamp in ${array_lamps[@]}
	do
		bri=$($DBUS_LOCATION/dbus-send --print-reply --system --dest=org.bluez /org/bluez/hci0/dev_${lamp}/service0023/char0029 org.bluez.GattCharacteristic1.ReadValue dict:string:variant: | grep 'array of bytes' -A 1 | tail -1 | tr -d ' ')
		bri="$(printf '%d' "0x${bri}")"
		if [[ "$bri" -gt "$max" ]]; then
			let max=$bri
		fi
	done
	echo $max
}

fade() {
	let current=$(get_brightness)
	#echo "cur: $current"
	let end=$1
	let dur=$2
	steps=$(echo "scale=0; $current/$dur" | bc)
	let c=0
	while [ "$c" -lt "$dur" ]; do
		bri="0x$(printf '%02x' "${current}")"
		set_brightness $bri
		let c=c+1
		let current=current-steps
		#echo "cur: $current"
		#echo $c
		sleep 1
	done
	set_brightness "1"
	send "0x00"
}
